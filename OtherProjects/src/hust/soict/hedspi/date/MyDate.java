package hust.soict.hedspi.date;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class MyDate {
	private int day, month, year;

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		if (day > 0 && day <= 31)
			this.day = day;
		else {
			System.out.println("Invalid day");
			System.exit(0);
		}
	}
	
//	public void setDay(String day) {
//		
//	}
	
	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		if (month >= 1 && month <= 12)
			this.month = month;
		else {
			System.out.println("Invalid month");
			System.exit(0);
		}
	}
	
//	public void setMonth(String month) {
//		
//	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		if (year > 0)
			this.year = year;
		else {
			System.out.println("Invalid year");
			System.exit(0);
		}
	}

//	public void setYear(String year) {
//		
//	}
	
	public MyDate() {
		super();
		LocalDate today = LocalDate.now();
		this.day = today.getDayOfMonth();
		this.month = today.getMonthValue();
		this.year = today.getYear();
	}

	public MyDate(int day, int month, int year) {
		super();
		this.setDay(day);
		this.setMonth(month);
		this.setYear(year);
	}
	
	public MyDate(String strToday) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate today = LocalDate.parse(strToday, formatter);
		this.day = today.getDayOfMonth();
		this.month = today.getMonthValue();
		this.year = today.getYear();
	}
	
	public void accept() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a Date (e.g: 18/10/2019): ");
		
		String strDate = sc.nextLine();
		
		sc.close();
		
		MyDate newDate = new MyDate(strDate);
		this.day = newDate.getDay();
		this.month = newDate.getMonth();
		this.year = newDate.getYear();
	}
	
	public void print() {
		System.out.printf("Today is %d %d %d\n", 
				this.month, this.day, this.year);
	}
}
