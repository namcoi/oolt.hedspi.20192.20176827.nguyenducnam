package hust.soict.hedspi.date;
import java.util.Date; 

public class Order {
	
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITTED_ORDERS = 5;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered;
	public static int nbOrders = 0;
	public Date dateOrdered = new Date();
	
	Order() {
		if(nbOrders < MAX_LIMITTED_ORDERS) {
			nbOrders++;
			System.out.println("New order has been added.");
		}
		else {
			System.out.println("Orders is full, cant add new order!!!");
		}
	}
	
	public Date getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(Date dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public int getQtyOrdered() {
		return qtyOrdered;
	}
	
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(this.qtyOrdered < MAX_NUMBERS_ORDERED) {
			this.itemsOrdered[qtyOrdered++] = disc;
			System.out.println("Disc has been added.");
		}
		else
			System.out.println("The order is already full.");
	};
	
	public void addDigitalVideoDisc(DigitalVideoDisc [] dvdList){
		if(this.qtyOrdered + dvdList.length <= MAX_NUMBERS_ORDERED) {
			int i;
			for(i=0;i<dvdList.length;i++) {
				this.itemsOrdered[qtyOrdered++] = dvdList[i];
				System.out.println("Disc" + (i+1) + "has been added.");
			}
		}
		else {
			System.out.println("The order is already full.");
		}
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2){
		if(this.qtyOrdered+1 < MAX_NUMBERS_ORDERED) {
			this.itemsOrdered[qtyOrdered++] = dvd1;
			this.itemsOrdered[qtyOrdered++] = dvd2;
			System.out.println("Disc has been added.");
		}
		else {
			System.out.println("The order is already full.");
		}
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		int i, j;
		boolean flag = false;
		for (i = 0; i < this.qtyOrdered; i++) {
			if (this.itemsOrdered[i].getTitle().equals(disc.getTitle())) {
				flag = true;
				for (j = i; j < this.qtyOrdered-1; j++)
					itemsOrdered[j] = itemsOrdered[j + 1];
				this.qtyOrdered--;
				System.out.println("The disc has been removed");
			}
		}
		if (!flag) System.out.println("Nothing to be done");
	}

	public float totalCost() {
		float sum = 0;
		for (int i = 0; i < this.qtyOrdered; i++) {
			sum += itemsOrdered[i].getCost();
		}
		return sum;
	}
	
	public void printList() {
		System.out.println("*********************Order**************** ********");
		System.out.println("Date: " + this.dateOrdered);
		System.out.println("Ordered Items: ");
		int i;
		for(i=0;i<this.qtyOrdered;i++) {
			System.out.println((i+1)+". DVD - " + itemsOrdered[i].getCategory() + " - " + itemsOrdered[i].getDirector() + " - " + itemsOrdered[i].getLength() + ": " + itemsOrdered[i].getCost() + "$");
		}
		System.out.println("Total cost: " + totalCost());
		System.out.println("**************************************************");
	}
	
}
