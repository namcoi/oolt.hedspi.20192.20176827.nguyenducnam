package hust.soict.hedspi.date;
import java.text.ParseException;

public class DateTest {

	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub
		MyDate newDate = new MyDate();
		newDate.print();
		
		MyDate newDate2 = new MyDate(1, 7, 1999);
		newDate2.print();
		
		newDate.accept();
		newDate.print();
		
		//Test comparing 2 dates
		DateUtils.compare(newDate, newDate2);
		
		//Test sort number of dates
		MyDate array[] = new MyDate[2];
		array[1] = newDate;
		array[0] = newDate2;
		DateUtils.sortDate(array);
	}
	
}
