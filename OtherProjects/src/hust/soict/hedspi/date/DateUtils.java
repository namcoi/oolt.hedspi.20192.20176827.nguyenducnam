package hust.soict.hedspi.date;
import java.util.Date;

import java.text.*;
import java.util.Arrays;

public class DateUtils {

	public static void compare(MyDate date1,MyDate date2) throws ParseException {
		
		SimpleDateFormat sdformat = new SimpleDateFormat("dd/MM/yyyy");
	    Date d1 = sdformat.parse(date1.getDay() + "/" + date1.getMonth() + "/" + date1.getYear());
	    Date d2 = sdformat.parse(date2.getDay() + "/" + date2.getMonth() + "/" + date2.getYear());
	    if(d1.compareTo(d2) > 0) {
	    	System.out.println("Date 1 occurs after Date 2");
	    } 
	    else if(d1.compareTo(d2) < 0) {
	    	System.out.println("Date 1 occurs before Date 2");
	    } 
	    else if(d1.compareTo(d2) == 0) {
	    	System.out.println("Both dates are equal");
	    }
	}

	public static void sortDate(MyDate Array[]) throws ParseException {
		int i;
		Date sortedArray[] = new Date[Array.length];
		SimpleDateFormat sdformat = new SimpleDateFormat("dd/MM/yyyy");
		for(i=0;i<Array.length;i++) {
			sortedArray[i] = sdformat.parse(Array[i].getDay() + "/" + Array[i].getMonth() + "/" + Array[i].getYear());
		}
		
		Arrays.sort(sortedArray);
		System.out.println("Sorted array of dates: ");
		for(Date date:sortedArray) {
			System.out.println(sdformat.format(date));
		}
	}
}
