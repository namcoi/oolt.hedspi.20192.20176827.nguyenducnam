package hust.soict.hedspi.lab02;


import javax.swing.JOptionPane;
public class Triangle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String strNum;
		int i,j;
		strNum = JOptionPane.showInputDialog(null,"Please input the height(n): ", "n = ",JOptionPane.INFORMATION_MESSAGE);
		int n = Integer.parseInt(strNum);
		for(i = 1; i <= n; i++) {
			int k = 0;
			for(j = 1;j <= n-i; j++) {
				System.out.print(" ");
			}
			while(k!=2*i-1) {
				System.out.print("*");
				k++;
			}
			System.out.print("\n");
		}
		System.exit(0);
	}
}
