package hust.soict.hedspi.lab02;


import java.util.Scanner;
public class Add2Matrices {

	static int[][] plus_mx(int [][] arr1, int [][] arr2) {
		int n = arr1.length;
		int m = arr1[0].length;
		int mx_sum[][] = new int[n][m];
		
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				mx_sum[i][j] = arr1[i][j] + arr2[i][j];
			}
		}
		
		return mx_sum;
	}
	
	static void prt_arr(int [][] array) {
		for (int[] x : array)
		{
		   for (int y : x)
		   {
		        System.out.print(y + " ");
		   }
		   System.out.println();
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter rows:");
		int n = sc.nextInt();
		System.out.println("Enter columns:");
		int m = sc.nextInt();
		
		int mx_1[][] = new int[n][m];
		int mx_2[][] = new int[n][m];
		
		
		System.out.println("enter elements of matrix 1");
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				mx_1[i][j] = sc.nextInt();
			}
		}
		
		
		System.out.println("enter elements of matrix 2");
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				mx_2[i][j] = sc.nextInt();
			}
		}
		sc.close();
		System.out.println("Sum of 2 matrixs is ");
		prt_arr(plus_mx(mx_1, mx_2));
		
	}

}
