package hust.soict.hedspi.lab02;


import java.util.Scanner;
import java.util.Arrays;
public class SortAndSum {

	public static int Sum(int[] arr,int n) {
		int sum=0;
		for(int i=0;i<n;i++) {
			sum+=arr[i];
		}
		return sum;
	}
	
	public static int Average(int[] arr,int n) {
		int avg=Sum(arr,n)/n;
		return avg;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int n;
		System.out.println("Enter the length of the array");
		n = sc.nextInt();
		int[] array = new int[n];
		for(int i=0;i<n;i++) {
			System.out.println("Enter number "+(i+1));
			array[i]=sc.nextInt();
		}
		sc.close();
		Arrays.sort(array);
		System.out.println("Sorted array: "+ Arrays.toString(array));
		System.out.println("Sum: " + Sum(array,array.length));
		System.out.println("Avg: " + Average(array,array.length));
	}
	
}
