package hust.soict.hedspi.lab02;


import java.util.Scanner;

public class DaysInMonth {
	private static Scanner sc;
	public static void main(String[] args) {
		int month,year;
		sc = new Scanner(System.in);
		
		System.out.print(" Please Enter Month Number from 1 to 12 (1 = Jan, and 12 = Dec) : ");
		month = sc.nextInt();	
		System.out.print(" Please Enter Year Number: ");
		year = sc.nextInt();
		if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12 )
		{
			System.out.println("\n" + month + "/" + year + " has 31 Days");  	
		}
		else if ( month == 4 || month == 6 || month == 9 || month == 11 )
		{
			System.out.println("\n" + month + "/" + year + " has 30 Days");  	
		}  
		else if ( month == 2 )
		{
			System.out.println("\n" + month + "/" + year + " has either 28 or 29 Days");  	
		} 
		else
			System.out.println("\n Please enter Valid Number between 1 to 12");
		System.exit(0);
	}
}