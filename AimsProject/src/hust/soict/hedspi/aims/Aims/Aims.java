package hust.soict.hedspi.aims.Aims;

import java.util.Scanner;

import hust.soict.hedspi.aims.Test.MemoryDaemon;
import hust.soict.hedspi.aims.media.Book;
import hust.soict.hedspi.aims.media.CompactDisc;
import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Track;
import hust.soict.hedspi.aims.order.Order;

public class Aims {
	private static Scanner sc = new Scanner(System.in);
	private static Order anOrder;
	
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}
	
	public static void addItem() {
		// test
		// Media newItem = new Media("title", "category", 1, 12);
		int choice;
		
		do {
			System.out.println("1. Book");
			System.out.println("2. CompactDisc");
			System.out.println("3. DigitalVideoDisc");
			System.out.println("--------------------------------");
			System.out.println("Please choose a number: 1-2-3");
			choice = sc.nextInt();
			sc.nextLine();
			switch (choice) {
			case 1:
				Book book1 = new Book("Harry Porter", "J.K.Rowling", 20);
				anOrder.addMedia(book1);
				break;
			case 2:
				CompactDisc disc1 = new CompactDisc("Boyce Avenue", 15);
				Track track1 = new Track();
				track1.setTitle("Memories");
				track1.setLength(10);
				Track track2 = new Track();
				track1.setTitle("Naked");
				track1.setLength(10);
				disc1.addTrack(track1);
				disc1.addTrack(track2);
				anOrder.addMedia(disc1);
				
				System.out.println("Do you want to play this track?(Yes/No)");
				String answer = sc.next();
				if(answer.equals("yes")) {
					disc1.play();
				}
				break;
			case 3:
				DigitalVideoDisc disc2 = new DigitalVideoDisc("20", "Pop", "Taylor Swift");
				anOrder.addMedia(disc2);
				break;
			default:
				break;
			}
		} while (choice > 3 || choice < 1);
		
		
	}

	public static void removeItem() {
		System.out.print("Enter the id: ");
		anOrder.removeMedia(sc.nextInt());
		sc.nextLine();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n;
		MemoryDaemon Deamon = new MemoryDaemon(); 
		Thread thread1 = new Thread(Deamon);
		thread1.setDaemon(true);
		thread1.start();
		
		while (true) {
			showMenu();
			n = sc.nextInt();
			sc.nextLine();
			switch (n) {
			case 1:
				anOrder = new Order();
				break;
			case 2:
				addItem();
				break;
			case 3:
				removeItem();
				break;
			case 4:
				anOrder.printList();
				break;
			case 0:
				sc.close();
				System.exit(0);
			default:
				break;
			}
		}
		
	}
}
