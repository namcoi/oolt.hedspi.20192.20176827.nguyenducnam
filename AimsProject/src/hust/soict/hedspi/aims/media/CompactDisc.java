package hust.soict.hedspi.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable{
	private String artist;
	private int length=0;
	private ArrayList<Track> tracks = new ArrayList<Track>();

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public void addTrack(Track track) {
		if(tracks.indexOf(track)==-1) {
			tracks.add(track);
		} else {
			System.out.print("The input track is already in the list of tracks inform users.");
		}
	}
	
	public void removeTrack(Track track) {
		if(tracks.indexOf(track)!=-1) {
			tracks.remove(track);
			System.out.print("The input track has been removed from the list of tracks.");
		} else {
			System.out.print("The input track does not exist in the list of tracks.");
		}
	}
	
	public int getLength() {
		int i,total = 0;
		for(i=0;i<tracks.size();i++) {
			total+=tracks.get(i).getLength();
		}
		return total;
	}
	
	public void play() {
		int i;
		for(i=0;i<tracks.size();i++) {
			tracks.get(i).play();
		}
	}
	
	public CompactDisc(String artist, int length) {
		this.artist = artist;
		this.length = length;
		
	}
}
