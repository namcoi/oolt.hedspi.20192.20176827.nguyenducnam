package hust.soict.hedspi.aims.media;

public class DigitalVideoDisc extends Disc implements Playable{
	private String director;
	private int length;
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public DigitalVideoDisc(String title) {
		super();
		this.title=title;
	}
	public DigitalVideoDisc(String title,String category) {
		super();
		this.title=title;
		this.category=category;
	}
	public DigitalVideoDisc(String title,String category,String director) {
		super();
		this.title=title;
		this.category=category;
		this.director=director;
	}
	public DigitalVideoDisc(String title,String category,String director, int length) {
		super();
		this.title=title;
		this.category=category;
		this.director=director;
		this.length=length;
	}
	public DigitalVideoDisc(String title,String category,String director, int length,float cost) {
		super();
		this.title=title;
		this.category=category;
		this.director=director;
		this.length=length;
		this.cost=cost;
	}
	
	public boolean search(String title) {
		String arr[] = title.split(" ");
		for(String str : arr) {
			if(this.title.contains(str)!=true) {
				return false;
			}
		}
		return true;
	}
	
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
}
