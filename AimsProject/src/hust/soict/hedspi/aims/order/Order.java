package hust.soict.hedspi.aims.order;
import java.util.ArrayList;
import java.util.Date;

import hust.soict.hedspi.aims.media.DigitalVideoDisc;
import hust.soict.hedspi.aims.media.Media; 

public class Order {
	
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITTED_ORDERS = 5;
	public static int nbOrders = 0;
	private int id;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Order(int id) {
		this.id = id;
	}

	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	public Date dateOrdered = new Date();
	
	public Order() {
		if(nbOrders < MAX_LIMITTED_ORDERS) {
			nbOrders++;
			System.out.println("New order has been added.");
		}
		else {
			System.out.println("Orders is full, cant add new order!!!");
		}
	}
	
	public void addMedia(Media media) {
		if(this.itemsOrdered.size() < MAX_NUMBERS_ORDERED) {
			this.itemsOrdered.add(media);
			System.out.println("Disc has been added.");
		}
		else
			System.out.println("The order is already full.");
	}
	
	public void removeMedia(Media media) {
		if(!this.itemsOrdered.isEmpty() && this.itemsOrdered.contains(media)) {
			this.itemsOrdered.remove(media);
		}
	}
	
	public void removeMedia(int id) {
		for(int i = 0; i<itemsOrdered.size(); i++) {
			if (itemsOrdered.get(i).getId() == id) {
				itemsOrdered.remove(itemsOrdered.get(i));
				return;
			}		
		}
	}
	
	public Date getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(Date dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public float totalCost() {
		float sum = 0;
		for (int i = 0; i < this.itemsOrdered.size(); i++) {
			sum += itemsOrdered.get(i).getCost();
		}
		return sum;
	}
	
	public void printList() {
		System.out.println("*********************Order**************** ********");
		System.out.println("Date: " + this.dateOrdered);
		System.out.println("Ordered Items: ");
		int i;
		for(i=0;i<this.itemsOrdered.size();i++) {
			System.out.println((i+1)+". " + itemsOrdered.get(i).getTitle() + " - " + itemsOrdered.get(i).getCategory()  + ": " + itemsOrdered.get(i).getCost() + "$");
		}
		System.out.println("Total cost: " + totalCost());
		System.out.println("**************************************************");
	}
	
	public DigitalVideoDisc getALuckyItem() {
		int i = (int)(Math.random() * this.itemsOrdered.size());
		System.out.println(i);
		return (DigitalVideoDisc) this.itemsOrdered.get(i);
	}
	
}
